#ifndef F_ENUMS_H
#define F_ENUMS_H

#include "futile_cst.h"
#include <stdbool.h>

F_DEFINE_TYPE(f_enumerator);

f90_f_enumerator_pointer f90_f_enumerator_copy_constructor(const f90_f_enumerator* other);

f90_f_enumerator_pointer f90_f_enumerator_type_new(f90_f_enumerator_pointer (*family),
  const int (*id),
  const char (*name)[64]);

void f90_f_enumerator_free(f90_f_enumerator_pointer self);

f90_f_enumerator_pointer f_enumerator_null(void);

void nullify_f_enum(f90_f_enumerator* en);

void f_enum_attr(f90_f_enumerator* dest,
  f90_f_enumerator* attr);

void f_enum_update(f90_f_enumerator* dest,
  const f90_f_enumerator* src);

bool enum_is_enum(const f90_f_enumerator* en,
  const f90_f_enumerator* en1);

bool enum_is_int(const f90_f_enumerator* en,
  int int_bn);

bool int_is_enum(int int_bn,
  const f90_f_enumerator* en);

bool enum_is_char(const f90_f_enumerator* en,
  const char* char_bn);

bool enum_is_not_enum(const f90_f_enumerator* en,
  const f90_f_enumerator* en1);

bool enum_is_not_int(const f90_f_enumerator* en,
  int int_bn);

bool enum_is_not_char(const f90_f_enumerator* en,
  const char* char_bn);

bool enum_has_attribute(const f90_f_enumerator* en,
  const f90_f_enumerator* family);

bool enum_has_char(const f90_f_enumerator* en,
  const char* family);

bool enum_has_int(const f90_f_enumerator* en,
  int family);

f90_f_enumerator_pointer enum_get_from_int(const f90_f_enumerator* en,
  int family);

f90_f_enumerator_pointer enum_get_from_char(const f90_f_enumerator* en,
  const char* family);

f90_f_enumerator_pointer enum_get_from_enum(const f90_f_enumerator* en,
  const f90_f_enumerator* family);

int int_enum(const f90_f_enumerator* en);

/* void char_enum(const f90_f_enumerator* en); */

#endif
