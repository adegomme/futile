#include "f_enums.h"
#include <config.h>
#include <string.h>

void FC_FUNC_(bind_f90_f_enumerator_copy_constructor, BIND_F90_F_ENUMERATOR_COPY_CONSTRUCTOR)(f90_f_enumerator_pointer*,
  const f90_f_enumerator*);
f90_f_enumerator_pointer f90_f_enumerator_copy_constructor(const f90_f_enumerator* other)
{
  f90_f_enumerator_pointer out_self;
  FC_FUNC_(bind_f90_f_enumerator_copy_constructor, BIND_F90_F_ENUMERATOR_COPY_CONSTRUCTOR)
    (&out_self, other);
  return out_self;
}

void FC_FUNC_(bind_f90_f_enumerator_type_new, BIND_F90_F_ENUMERATOR_TYPE_NEW)(f90_f_enumerator_pointer*,
  f90_f_enumerator_pointer*,
  const int*,
  const char*,
  size_t);
f90_f_enumerator_pointer f90_f_enumerator_type_new(f90_f_enumerator_pointer (*family),
  const int (*id),
  const char (*name)[64])
{
  f90_f_enumerator_pointer out_self;
  size_t name_len = 64, name_chk_len = 64;
  FC_FUNC_(bind_f90_f_enumerator_type_new, BIND_F90_F_ENUMERATOR_TYPE_NEW)
    (&out_self, family, id, name ? *name : NULL, name_chk_len);
  return out_self;
}

void FC_FUNC_(bind_f90_f_enumerator_free, BIND_F90_F_ENUMERATOR_FREE)(f90_f_enumerator_pointer*);
void f90_f_enumerator_free(f90_f_enumerator_pointer self)
{
  FC_FUNC_(bind_f90_f_enumerator_free, BIND_F90_F_ENUMERATOR_FREE)
    (&self);
}

void FC_FUNC_(bind_f_enumerator_null, BIND_F_ENUMERATOR_NULL)(f90_f_enumerator_pointer*);
f90_f_enumerator_pointer f_enumerator_null(void)
{
  f90_f_enumerator_pointer out_en;
  FC_FUNC_(bind_f_enumerator_null, BIND_F_ENUMERATOR_NULL)
    (&out_en);
  return out_en;
}

void FC_FUNC_(bind_nullify_f_enum, BIND_NULLIFY_F_ENUM)(f90_f_enumerator*);
void nullify_f_enum(f90_f_enumerator* en)
{
  FC_FUNC_(bind_nullify_f_enum, BIND_NULLIFY_F_ENUM)
    (en);
}

void FC_FUNC_(bind_f_enum_attr, BIND_F_ENUM_ATTR)(f90_f_enumerator*,
  f90_f_enumerator*);
void f_enum_attr(f90_f_enumerator* dest,
  f90_f_enumerator* attr)
{
  FC_FUNC_(bind_f_enum_attr, BIND_F_ENUM_ATTR)
    (dest, attr);
}

void FC_FUNC_(bind_f_enum_update, BIND_F_ENUM_UPDATE)(f90_f_enumerator*,
  const f90_f_enumerator*);
void f_enum_update(f90_f_enumerator* dest,
  const f90_f_enumerator* src)
{
  FC_FUNC_(bind_f_enum_update, BIND_F_ENUM_UPDATE)
    (dest, src);
}

void FC_FUNC_(bind_enum_is_enum, BIND_ENUM_IS_ENUM)(int*,
  const f90_f_enumerator*,
  const f90_f_enumerator*);
bool enum_is_enum(const f90_f_enumerator* en,
  const f90_f_enumerator* en1)
{
  int out_ok;
  FC_FUNC_(bind_enum_is_enum, BIND_ENUM_IS_ENUM)
    (&out_ok, en, en1);
  return out_ok;
}

void FC_FUNC_(bind_enum_is_int, BIND_ENUM_IS_INT)(int*,
  const f90_f_enumerator*,
  const int*);
bool enum_is_int(const f90_f_enumerator* en,
  int int_bn)
{
  int out_ok;
  FC_FUNC_(bind_enum_is_int, BIND_ENUM_IS_INT)
    (&out_ok, en, &int_bn);
  return out_ok;
}

void FC_FUNC_(bind_int_is_enum, BIND_INT_IS_ENUM)(int*,
  const int*,
  const f90_f_enumerator*);
bool int_is_enum(int int_bn,
  const f90_f_enumerator* en)
{
  int out_ok;
  FC_FUNC_(bind_int_is_enum, BIND_INT_IS_ENUM)
    (&out_ok, &int_bn, en);
  return out_ok;
}

void FC_FUNC_(bind_enum_is_char, BIND_ENUM_IS_CHAR)(int*,
  const f90_f_enumerator*,
  const char*,
  const size_t*,
  size_t);
bool enum_is_char(const f90_f_enumerator* en,
  const char* char_bn)
{
  int out_ok;
  size_t char_bn_chk_len, char_bn_len = char_bn_chk_len = char_bn ? strlen(char_bn) : 0;
  FC_FUNC_(bind_enum_is_char, BIND_ENUM_IS_CHAR)
    (&out_ok, en, char_bn, &char_bn_len, char_bn_chk_len);
  return out_ok;
}

void FC_FUNC_(bind_enum_is_not_enum, BIND_ENUM_IS_NOT_ENUM)(int*,
  const f90_f_enumerator*,
  const f90_f_enumerator*);
bool enum_is_not_enum(const f90_f_enumerator* en,
  const f90_f_enumerator* en1)
{
  int out_ok;
  FC_FUNC_(bind_enum_is_not_enum, BIND_ENUM_IS_NOT_ENUM)
    (&out_ok, en, en1);
  return out_ok;
}

void FC_FUNC_(bind_enum_is_not_int, BIND_ENUM_IS_NOT_INT)(int*,
  const f90_f_enumerator*,
  const int*);
bool enum_is_not_int(const f90_f_enumerator* en,
  int int_bn)
{
  int out_ok;
  FC_FUNC_(bind_enum_is_not_int, BIND_ENUM_IS_NOT_INT)
    (&out_ok, en, &int_bn);
  return out_ok;
}

void FC_FUNC_(bind_enum_is_not_char, BIND_ENUM_IS_NOT_CHAR)(int*,
  const f90_f_enumerator*,
  const char*,
  const size_t*,
  size_t);
bool enum_is_not_char(const f90_f_enumerator* en,
  const char* char_bn)
{
  int out_ok;
  size_t char_bn_chk_len, char_bn_len = char_bn_chk_len = char_bn ? strlen(char_bn) : 0;
  FC_FUNC_(bind_enum_is_not_char, BIND_ENUM_IS_NOT_CHAR)
    (&out_ok, en, char_bn, &char_bn_len, char_bn_chk_len);
  return out_ok;
}

void FC_FUNC_(bind_enum_has_attribute, BIND_ENUM_HAS_ATTRIBUTE)(int*,
  const f90_f_enumerator*,
  const f90_f_enumerator*);
bool enum_has_attribute(const f90_f_enumerator* en,
  const f90_f_enumerator* family)
{
  int out_ok;
  FC_FUNC_(bind_enum_has_attribute, BIND_ENUM_HAS_ATTRIBUTE)
    (&out_ok, en, family);
  return out_ok;
}

void FC_FUNC_(bind_enum_has_char, BIND_ENUM_HAS_CHAR)(int*,
  const f90_f_enumerator*,
  const char*,
  const size_t*,
  size_t);
bool enum_has_char(const f90_f_enumerator* en,
  const char* family)
{
  int out_ok;
  size_t family_chk_len, family_len = family_chk_len = family ? strlen(family) : 0;
  FC_FUNC_(bind_enum_has_char, BIND_ENUM_HAS_CHAR)
    (&out_ok, en, family, &family_len, family_chk_len);
  return out_ok;
}

void FC_FUNC_(bind_enum_has_int, BIND_ENUM_HAS_INT)(int*,
  const f90_f_enumerator*,
  const int*);
bool enum_has_int(const f90_f_enumerator* en,
  int family)
{
  int out_ok;
  FC_FUNC_(bind_enum_has_int, BIND_ENUM_HAS_INT)
    (&out_ok, en, &family);
  return out_ok;
}

void FC_FUNC_(bind_enum_get_from_int, BIND_ENUM_GET_FROM_INT)(f90_f_enumerator_pointer*,
  const f90_f_enumerator*,
  const int*);
f90_f_enumerator_pointer enum_get_from_int(const f90_f_enumerator* en,
  int family)
{
  f90_f_enumerator_pointer out_iter;
  FC_FUNC_(bind_enum_get_from_int, BIND_ENUM_GET_FROM_INT)
    (&out_iter, en, &family);
  return out_iter;
}

void FC_FUNC_(bind_enum_get_from_char, BIND_ENUM_GET_FROM_CHAR)(f90_f_enumerator_pointer*,
  const f90_f_enumerator*,
  const char*,
  const size_t*,
  size_t);
f90_f_enumerator_pointer enum_get_from_char(const f90_f_enumerator* en,
  const char* family)
{
  f90_f_enumerator_pointer out_iter;
  size_t family_chk_len, family_len = family_chk_len = family ? strlen(family) : 0;
  FC_FUNC_(bind_enum_get_from_char, BIND_ENUM_GET_FROM_CHAR)
    (&out_iter, en, family, &family_len, family_chk_len);
  return out_iter;
}

void FC_FUNC_(bind_enum_get_from_enum, BIND_ENUM_GET_FROM_ENUM)(f90_f_enumerator_pointer*,
  const f90_f_enumerator*,
  const f90_f_enumerator*);
f90_f_enumerator_pointer enum_get_from_enum(const f90_f_enumerator* en,
  const f90_f_enumerator* family)
{
  f90_f_enumerator_pointer out_iter;
  FC_FUNC_(bind_enum_get_from_enum, BIND_ENUM_GET_FROM_ENUM)
    (&out_iter, en, family);
  return out_iter;
}

void FC_FUNC_(bind_int_enum, BIND_INT_ENUM)(int*,
  const f90_f_enumerator*);
int int_enum(const f90_f_enumerator* en)
{
  int out_int_enum;
  FC_FUNC_(bind_int_enum, BIND_INT_ENUM)
    (&out_int_enum, en);
  return out_int_enum;
}

