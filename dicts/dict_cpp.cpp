#include "Dict"

using namespace Futile;

Dictionary::Dictionary()
{
  dict_init(&mDict);
}

Dictionary::Dictionary(f90_dictionary_pointer source)
  : mDict(source)
{
}

Dictionary::~Dictionary()
{
  dict_free(&mDict);
}

void Dictionary::set(const char* key ,const char* value)
{
  dict_set_string(&mDict, key, value);
}
void Dictionary::set(const char* key ,const double * array, size_t len)
{
  dict_set_double_array(&mDict, key, array, len);
}
void Dictionary::set(const char* key ,double value)
{
  dict_set_double(&mDict, key, value);
}
void Dictionary::set(const char* key , const double * array, size_t lenx, size_t leny)
{
  dict_set_double_matrix(&mDict, key, array, lenx, leny);
}
void Dictionary::set(const char* key , Dictionary &value)
{
  dict_set_dict(&mDict, key, &value.mDict);
}

void Dictionary::add(Dictionary &value)
{
  dict_add_dict(&mDict, &value.mDict);
}
void Dictionary::add(int value)
{
  dict_add_int(&mDict, value);
}
void Dictionary::add(double value)
{
  dict_add_double(&mDict, value);
}
void Dictionary::add(const char *value)
{
  dict_add_string(&mDict, value);
}

bool Dictionary::get(const char* key , double * array, size_t len)
{
  return dict_get_double_array(&mDict, key, array, len);
}
bool Dictionary::get(const char* key , Dictionary *dict)
{
  f90_dictionary_pointer out;
  if (dict_get_dict(&mDict, key, &out)) {
    if (dict) {
      dict = new Dictionary(out);
    } else {
      dict_free(&out);
    }
    return true;
  }
  return false;
}

Dictionary::Iter::Iter(const Dictionary &parent)
  : key(mIter.key), value(mIter.value)
{
  dict_iter_new(&mIter, &parent.mDict);
}

bool Dictionary::Iter::next()
{
  return iterate(&mIter);
}

void Dictionary::initialize()
{
  futile_dicts_initialize();
}

void Dictionary::finalize()
{
  futile_dicts_finalize();
}
